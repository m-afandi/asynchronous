let content = document.getElementById('content');
const path = "https://jsonplaceholder.typicode.com/users";
let row = "";
const getData = (url, callback) => {
  let request = new XMLHttpRequest();
  request.onload = function() {
    if(request.status === 200) {
      return callback(JSON.parse(request.responseText))
    }
  };
  request.open("GET", url);
  request.send();
}

const data = getData(path, function(element) {
  setTimeout(() => {
    element.forEach(e => {
      row+= `<tr>
      <td>${e["id"]}</td>
      <td>${e["name"]}</td>
      <td>${e["username"]}</td>
      <td>${e["email"]}</td>
      <td>
        ${e["address"]["street"]}, ${e["address"]["suite"]}, ${e["address"]["city"]}
      </td>
      <td>${e["company"]["name"]}</td></tr>`
    });
    content.innerHTML = row;
  }, 1500)
  content.innerHTML = '<tr><td colspan=6 align=center>Loading...</td></tr>';
});